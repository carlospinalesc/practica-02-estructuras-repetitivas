using System;

namespace practica_02_estructuras_repetitivas_master
{
    class Ejercicio1
    {
        static void Main(string[] args)
        {
            //1.	Crea un programa que escriba en pantalla los números del 1 al 10, "while". 

           int x=0;
      
         while(x<=10)
         Console.WriteLine( x++);
               
           Console.WriteLine("FIN DE LA APLICACION");


        }
    }
}
