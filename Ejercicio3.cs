using System;

namespace practica_02_estructuras_repetitivas_master
{
    class Ejercicio3
    {
        static void Main(string[] args)
        {
            /*3- Crear un programa que dé al usuario
             tres oportunidades para adivinar un número del 1 al 23. */

             int numero, numero1, contador;
        
         numero=13;
         contador=0;
        
         while (contador < 3)
            {
            Console.Write("Adivina un número del 1 al 23: ");
            numero1=Convert.ToInt32(Console.ReadLine());
        
            if (numero1 != numero)
                Console.WriteLine("No has adivinado el número.");
            
            else
                Console.WriteLine("Has adivinado el número.");

            contador++;
            }

        }
    }
}