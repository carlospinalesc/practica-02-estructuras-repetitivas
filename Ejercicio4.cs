using System;

namespace practica_02_estructuras_repetitivas_master
{
    class Ejercicio4
    {
        static void Main(string[] args)
        {
            /*4.  Crea un programa que escriba en pantalla los números del 1 al 10, usando "do..while". */

            int Numero = 0;
             do
            {
           Numero += 1;
           Console.WriteLine("Resultado " + Numero);
  
            }while (Numero < 10);


        }
    }
}