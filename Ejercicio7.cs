using System;

namespace practica_02_estructuras_repetitivas_master
{
    class Ejercicio7
    {
        static void Main(string[] args)
        {
            /*7. Crear un programa que muestre los primeros ocho números pares
            (tips: en cada pasada habrá que aumentar de 2 en 2, o bien mostrar el doble del valor que hace de contador).*/

            int AD;
            for (AD=2; AD<=20; AD=AD+2)
            Console.WriteLine("Numero actual {0}", AD);

        }
    }
}